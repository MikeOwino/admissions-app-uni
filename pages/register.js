import React, { useState } from "react";
import { useRouter } from "next/router";
import { Form, Input, Button, Checkbox } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
// import "antd/dist/antd.css";
import Link from "next/link";
import mario from "../public/images/logos.webp";
import Image from "next/image";

const Register = () => {
  const [form] = Form.useForm();
  const [error, setError] = useState(null);
  const router = useRouter();

  const onFinish = async (values) => {
    // Perform registration here
    // Example:
    // try {
    //   const res = await authService.register(values.username, values.email, values.password);
    //   if (res.status === 201) {
    //     router.push('/login');
    //   }
    // } catch (err) {
    //   setError(err.message);
    // }
    router.push("/login");
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const { email, password } = formData;
      await auth0.signup({
        email: email,
        password: password,
        connection: "Username-Password-Authentication",
      });
      router.push("/login");
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="register-container">
      <div className="register-form-container">
        <div className="logo-container">
          <Image src={mario} alt="logo" />
        </div>
        <Form
          form={form}
          name="normal_register"
          className="register-form"
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              {
                type: "email",
                message: "The input is not valid E-mail!",
              },
              {
                required: true,
                message: "Please input your E-mail!",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
            hasFeedback
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item
            name="confirm"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please confirm your password!",
              },
              {
                validator: (_, value) =>
                  value === form.getFieldValue("password") ||
                  "The two passwords that you entered do not match!",
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              placeholder="Confirm Password"
            />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="register-form-button"
            >
              Register
            </Button>
            Or <Link href="/login">login now!</Link>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default Register;
