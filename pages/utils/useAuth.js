import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import auth0 from '../lib/auth0';

export const useAuth = () => {
  const [user, setUser] = useState();
  const router = useRouter();

  useEffect(() => {
    const getUser = async () => {
      try {
        const session = await auth0.getSession();
        if (!session) {
          router.push('/login');
          return;
        }
        setUser(session.user);
      } catch (error) {
        console.error(error);
        router.push('/login');
      }
    };
    getUser();
  }, [router]);

  return { user };
};
