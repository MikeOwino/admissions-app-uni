import React, { useState } from "react";
import { useRouter } from "next/router";
import { Form, Input, Button, Checkbox } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import mario from "../public/images/logos.webp";
import Image from "next/image";
// import "antd/dist/antd.css";
import Link from "next/link";

const Login = () => {
  const [form] = Form.useForm();
  const [error, setError] = useState(null);
  const router = useRouter();

  const onFinish = async (values) => {
    // Perform authentication here
    // Example:
    try {
      const res = await authService.login(values.username, values.password);
      if (res.status === 200) {
        router.push("/dashboard");
      }
    } catch (err) {
      setError(err.message);
    }
    router.push("/dashboard");
  };

  return (
    <div className="login-container">
      <div className="login-form-container">
        <div className="logo-container">
          <Image src={mario} alt="logo" />
        </div>
        <Form
          form={form}
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Please input your Username or Email!",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username or Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your Password!" }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <a className="login-form-forgot" href="">
              Forgot password
            </a>
          </Form.Item>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Log in
            </Button>
            Or <Link href="/register">register now!</Link>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default Login;
