import React from "react";
import { useAuth } from "./utils/useAuth";

const Dashboard = () => {
  const { user } = useAuth();

  if (!user) return <div>Loading...</div>;

  return (
    <div className="dashboard-container">
      <h1>Welcome {user.username}</h1>
      <p>Here&apos;s some information about your account:</p>
      <ul>
        <li>Email: {user.email}</li>
        <li>Account created at: {user.createdAt}</li>
      </ul>
    </div>
  );
};

export default Dashboard;
